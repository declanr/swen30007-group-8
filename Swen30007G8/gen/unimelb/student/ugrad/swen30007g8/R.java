/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package unimelb.student.ugrad.swen30007g8;

public final class R {
    public static final class array {
        public static final int helpString=0x7f080000;
    }
    public static final class attr {
        /** <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cancelLabel=0x7f010001;
        /** <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int okLabel=0x7f010000;
    }
    public static final class color {
        public static final int Black=0x7f070000;
        public static final int DarkBlue=0x7f070003;
        public static final int DarkGreen=0x7f070004;
        public static final int DarkRed=0x7f070001;
        public static final int LightBlue=0x7f070005;
        public static final int White=0x7f070002;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f060000;
        public static final int activity_vertical_margin=0x7f060001;
    }
    public static final class drawable {
        public static final int ic_explain_about=0x7f020000;
        public static final int ic_launcher=0x7f020001;
    }
    public static final class id {
        public static final int LinearLayout02=0x7f0a0011;
        public static final int LinearLayout03=0x7f0a0018;
        public static final int LinearLayout1=0x7f0a0003;
        public static final int LinearLayout1layout_width=0x7f0a0019;
        public static final int action_settings=0x7f0a0035;
        public static final int addCarer=0x7f0a001e;
        public static final int alert_hearder=0x7f0a0013;
        public static final int alerts=0x7f0a0014;
        public static final int backButton=0x7f0a0004;
        public static final int btnAlertScreen=0x7f0a000d;
        public static final int btnBack=0x7f0a0006;
        public static final int btnHelp=0x7f0a0010;
        public static final int btnManage=0x7f0a000f;
        public static final int btnOK=0x7f0a000e;
        public static final int btnPush=0x7f0a0022;
        public static final int btnRequests=0x7f0a0017;
        public static final int btn_add=0x7f0a0002;
        public static final int btn_help=0x7f0a000c;
        public static final int btn_login=0x7f0a000a;
        public static final int btn_mainMenu=0x7f0a0029;
        public static final int btn_register=0x7f0a000b;
        public static final int carerEmail=0x7f0a0001;
        public static final int comms=0x7f0a0016;
        public static final int comms_header=0x7f0a0015;
        public static final int contentTextFields=0x7f0a002c;
        public static final int email=0x7f0a0008;
        public static final int emailPush=0x7f0a0021;
        public static final int helpText=0x7f0a0005;
        public static final int isCarer=0x7f0a0026;
        public static final int linkedAP=0x7f0a001f;
        public static final int linkedCarers=0x7f0a001b;
        public static final int linked_header=0x7f0a001a;
        public static final int logOut=0x7f0a0012;
        public static final int login_title=0x7f0a0007;
        public static final int name=0x7f0a0024;
        public static final int okCancelBar=0x7f0a0032;
        public static final int orrequired1=0x7f0a002e;
        public static final int pass=0x7f0a0009;
        public static final int passconfirm=0x7f0a0025;
        public static final int password1=0x7f0a002f;
        public static final int password2=0x7f0a0030;
        public static final int pendingAP=0x7f0a0020;
        public static final int pendingCarers=0x7f0a001d;
        public static final int pending_header=0x7f0a001c;
        public static final int rdbtnAP=0x7f0a0028;
        public static final int rdbtnCarer=0x7f0a0027;
        public static final int required=0x7f0a002d;
        public static final int textPush=0x7f0a0023;
        public static final int title=0x7f0a0000;
        public static final int url=0x7f0a0031;
        public static final int user_id=0x7f0a002b;
        public static final int user_name=0x7f0a002a;
        public static final int widget_okcancelbar_cancel=0x7f0a0033;
        public static final int widget_okcancelbar_ok=0x7f0a0034;
    }
    public static final class layout {
        public static final int activity_add_carer=0x7f030000;
        public static final int activity_alert=0x7f030001;
        public static final int activity_help=0x7f030002;
        public static final int activity_login=0x7f030003;
        public static final int activity_main=0x7f030004;
        public static final int activity_main_ap=0x7f030005;
        public static final int activity_main_carer=0x7f030006;
        public static final int activity_manage_ap=0x7f030007;
        public static final int activity_manage_carer=0x7f030008;
        public static final int activity_push_test=0x7f030009;
        public static final int activity_register=0x7f03000a;
        public static final int list_item=0x7f03000b;
        public static final int main=0x7f03000c;
        public static final int widget_ok_cancel_bar=0x7f03000d;
    }
    public static final class menu {
        public static final int main=0x7f090000;
    }
    public static final class string {
        public static final int accept=0x7f040013;
        public static final int action_settings=0x7f040015;
        public static final int addCarer=0x7f040033;
        public static final int add_carer=0x7f040020;
        public static final int alert=0x7f040018;
        public static final int alert_header=0x7f040031;
        public static final int alert_sent=0x7f040019;
        public static final int alert_title=0x7f040017;
        public static final int app_name=0x7f040000;
        public static final int assisted_person=0x7f04002d;
        public static final int back=0x7f04001a;
        public static final int cancel=0x7f04001e;
        public static final int carer=0x7f04002c;
        public static final int comms_header=0x7f040030;
        public static final int email_hint=0x7f040024;
        public static final int enter_carer_email=0x7f040032;
        public static final int global_about=0x7f040002;
        public static final int global_accept=0x7f04000a;
        public static final int global_address=0x7f040009;
        public static final int global_cancel=0x7f040003;
        public static final int global_email=0x7f040007;
        public static final int global_name=0x7f040005;
        public static final int global_password=0x7f040004;
        public static final int global_phone=0x7f040006;
        public static final int global_submit=0x7f040001;
        public static final int global_website=0x7f040008;
        public static final int hello_world=0x7f040016;
        public static final int help=0x7f04001d;
        public static final int im_ok=0x7f04001b;
        public static final int linked_header=0x7f04002e;
        public static final int login=0x7f040023;
        public static final int logout=0x7f04001f;
        public static final int main_menu=0x7f040026;
        public static final int manage_carers=0x7f04001c;
        public static final int name_hint=0x7f04002b;
        public static final int pass_confirm=0x7f04002a;
        public static final int pass_hint=0x7f040025;
        public static final int pending_header=0x7f04002f;
        public static final int pushTest=0x7f040034;
        public static final int reg=0x7f040029;
        public static final int register=0x7f040028;
        public static final int reject=0x7f040014;
        public static final int remove_carer=0x7f040021;
        public static final int title=0x7f040022;
        public static final int validator_alnum=0x7f04000f;
        public static final int validator_confirm=0x7f04000e;
        public static final int validator_email=0x7f04000b;
        public static final int validator_empty=0x7f04000c;
        public static final int validator_hex=0x7f040010;
        public static final int validator_phone=0x7f040012;
        public static final int validator_regexp=0x7f040011;
        public static final int validator_url=0x7f04000d;
        public static final int view_requests=0x7f040027;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f050001;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f050002;
        public static final int Validator_Light=0x7f050000;
    }
    public static final class styleable {
        /** Attributes that can be used with a OkCancelBar.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #OkCancelBar_cancelLabel unimelb.student.ugrad.swen30007g8:cancelLabel}</code></td><td></td></tr>
           <tr><td><code>{@link #OkCancelBar_okLabel unimelb.student.ugrad.swen30007g8:okLabel}</code></td><td></td></tr>
           </table>
           @see #OkCancelBar_cancelLabel
           @see #OkCancelBar_okLabel
         */
        public static final int[] OkCancelBar = {
            0x7f010000, 0x7f010001
        };
        /**
          <p>This symbol is the offset where the {@link unimelb.student.ugrad.swen30007g8.R.attr#cancelLabel}
          attribute's value can be found in the {@link #OkCancelBar} array.


          <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name unimelb.student.ugrad.swen30007g8:cancelLabel
        */
        public static final int OkCancelBar_cancelLabel = 1;
        /**
          <p>This symbol is the offset where the {@link unimelb.student.ugrad.swen30007g8.R.attr#okLabel}
          attribute's value can be found in the {@link #OkCancelBar} array.


          <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name unimelb.student.ugrad.swen30007g8:okLabel
        */
        public static final int OkCancelBar_okLabel = 0;
    };
}
