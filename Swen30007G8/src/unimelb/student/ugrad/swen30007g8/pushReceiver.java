/**********************************************************
 * pushReceiver
 * 
 * This class serves to process the push notifications 
 * received by a user
 *********************************************************/

package unimelb.student.ugrad.swen30007g8;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/***********************************
* Push Receiver Class to get push
* notification and to process it
* A Receiver Class should be defined
* in the Manifest file
***********************************/

public class pushReceiver extends BroadcastReceiver{
	
	/***********************************
	* Override onReceive method to 
	* process Parse push notifications
	***********************************/
	
	@Override
	public void onReceive(Context context, Intent intent){
		
		/***********************************
		* Use Intent to get message from
		* Parse push notification
		***********************************/
		
		Bundle extras = intent.getExtras();
		String message;
		
		if (extras != null){
			message = extras.getString("com.parse.Data");
		}
		else{
			message = "";
		}
		
		/***********************************
		* Pop up a toast in order to show
		* the message, and writes it to file
		* in local storage
		***********************************/
		
		JSONObject object;
		try{
			object = new JSONObject(message);
			Toast toast = Toast.makeText(context, object.getString("alert"), Toast.LENGTH_SHORT);
			toast.show();
			xmlWriter(context, object);
		} catch(JSONException e){
			e.printStackTrace();
		}
		

		
		
	}
	
	/***************************************************
	 * This method writes the current message recieved
	 * to either alerts.xml or otherComm.xml, depending
	 * on what type of communication they are
	 ***************************************************/
	private void xmlWriter(Context context, JSONObject message)
	{
		File file;
		try
		{
			FileWriter out;
			
			//Gets the type of the message sent
			String messageType = message.getString("title").substring(0, message.getString("title").length()-" message".length());
			
			//Writes alerts to file as xml
			if (messageType.equals("Alert"))
			{
				file  = new File(context.getFilesDir(), "alerts.xml");
				out = new FileWriter(file);
				if (!file.exists())
				{
					file.createNewFile();
					out.write("<?xml version=\"1.0\" encoding =\"utf-8\"?>\n");
				}
				
				String ap;
				boolean cancel;
				if (message.getString("alert").contains("cancelled"))
				{
					ap = message.getString("alert").substring(0, message.getString("alert").length()-25);
					cancel = true;
				}
				else
				{
					ap = message.getString("alert").substring(0, message.getString("alert").length()-23);
					cancel = false;
				}
				
				
				
				out.append("<alert>\n <assistedperson>"+ap+"</assistedperson>\n<type>");
				
				if (cancel)
				{
					out.append("cancel");
				}
				else
				{
					out.append("actual");
				}
				out.append("</type>\n</alert>\n");
				out.close();
			}
			
			//Writes ok and not ok messages to file as xml
			else if ((messageType.equals("OK"))||(messageType.equals("Not OK")))
			{
				file  = new File(context.getFilesDir(), "otherComm.xml");
				out = new FileWriter(file);
				if (!file.exists())
				{
					file.createNewFile();
					out.write("<?xml version=\"1.0\" encoding =\"utf-8\"?>\n");
				}
				
				String ap;
				
				if (messageType.equals("OK"))
				{
					ap = message.getString("alert").substring(0, message.getString("alert").length()-17);
				}
				else
				{
					ap = message.getString("alert").substring(0, message.getString("alert").length()-38);
				}
				
				out.append("<comm>\n<assistedperson>"+ap+"</assistedperson>\n<type>"+messageType+"</type>\n</comm>\n");
				out.close();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();	
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
}
