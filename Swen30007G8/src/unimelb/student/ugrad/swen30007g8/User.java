/**********************************************************
 * User
 * 
 * This class serves to provide logic for User-related
 * functionality such as Logging In and Redirecting to
 * the appropriate MenuActivity
 *********************************************************/

package unimelb.student.ugrad.swen30007g8;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

public class User extends ParseUser{
	/********************************************
	 * Calls the multithreaded login function for
	 * two EditText parameters, and returns the 
	 * result into the LogInCallback parameter
	 ********************************************/
	public static void checkLogin(EditText email, EditText password, final LogInCallback callback) {
		ParseUser.logInInBackground(email.getText().toString(), password.getText().toString(), new LogInCallback() {
			public void done(ParseUser user, ParseException e) {
				callback.done(user, e);
			}
		});
		
	}

	/*************************************************
	 * Function to be called inside the LogInCallback
	 * that is taken as input to checkLogin().
	 * Transitions to the correct MenuActivity based 
	 * on whether the currently logged in user is a
	 * Carer or an AP.
	 *************************************************/
	public static void redirect(ParseUser current, Activity activity, ParseException e) {
		if (current != null) {
			Toast.makeText(activity.getApplicationContext(), "Login successful", Toast.LENGTH_SHORT).show();
			Intent mainActivity;
			if (current.getBoolean("isCarer")) {
				mainActivity = new Intent(activity.getApplicationContext(), CarerMenuActivity.class);
			}
			else {
				mainActivity = new Intent(activity.getApplicationContext(), APMenuActivity.class);
			}
			mainActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			activity.getApplicationContext().startActivity(mainActivity);
		}
		else {
			Toast.makeText(activity.getApplicationContext(), "Login failed", Toast.LENGTH_SHORT).show();
		}
	}
	


}