/**********************************************************
 * LoginActivity
 * 
 * This class serves to set up the login screen for the
 * user to interact with. It will also check that the
 * user has entered information into the fields correctly
 * and will change to the main menu screen for a carer
 * or assisted person if the login was successful
 *********************************************************/

package unimelb.student.ugrad.swen30007g8;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;

import com.parse.LogInCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.PushService;
import com.throrinstudio.android.common.libs.validator.Form;
import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validator.EmailValidator;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;

public class LoginActivity extends Activity {
	
	/***************************************************
	 * Variables declared for validation and debugging
	 ***************************************************/

	private Form mForm;
	private EditText email;
	private EditText password;
	private Validate emailField;
	private EmailValidator emailVal;
	final Context ctx = this;
		
	/***************************************************
	 * When the activity is created, this method serves 
	 * to set up the login fields for the user with 
	 * validation involved. Test cases can also be run via 
	 * this method if desired
	 ***************************************************/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		ParseFunctions.parseInit(this);
		ParseAnalytics.trackAppOpened(getIntent());
		
		//PushService.setDefaultPushCallback(this,PushRedirectActivity.class);
		//ParseInstallation.getCurrentInstallation().saveInBackground();
		
		this.mForm = new Form();

		email = (EditText) findViewById(R.id.email);
		password = (EditText) findViewById(R.id.pass);
		//allow people to press enter to trigger the button
		password.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keycode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_DOWN)
		        {
		            switch (keycode)
		            {
		                case KeyEvent.KEYCODE_DPAD_CENTER:
		                case KeyEvent.KEYCODE_ENTER:
		                    validateForm.onClick(v);
		                    return true;
		                default:
		                    break;
		            }
		        }
		        return false;
			}
		});
		emailField = new Validate(email);
		

		emailVal = new EmailValidator(this);

		emailField.addValidator(new NotEmptyValidator(this));
		emailField.addValidator(emailVal);

	}
	
	/***************************************************
	 * When this application is currently running, a 
	 * listener will be bound to the login button in 
	 * this method
	 ***************************************************/

	@Override
	protected void onResume() {
		super.onResume();

		mForm.addValidates(emailField);
		Button log = (Button) findViewById(R.id.btn_login);
		log.setOnClickListener(validateForm);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		return true;
	}
	
	/***************************************************
	 * This method will transition to the Main Menu, 
	 * however this will be removed in the final iteration 
	 * as this is mainly for testing purposes. In reality
	 * the user will have to login to get to the main menu
	 ***************************************************/
	
	public void getMainMenu(View view) {
		Intent menuActivity = new Intent(getApplicationContext(),
				APMenuActivity.class);
		startActivity(menuActivity);
	}
	
	/***************************************************
	 * If the user hasn't registered with our server, 
	 * then they can tap on the Register button to go
	 * to the Register screen
	 ***************************************************/

	public void btn_register(View view) {
		Intent registerActivity = new Intent(getApplicationContext(),
				RegisterActivity.class);
		startActivity(registerActivity);
	}
	
	/***************************************************
	 * Brings up a Help screen when the Help button is
	 * pressed
	 ***************************************************/
	public void getHelpScreen(View view){
		Intent helpScreen = new Intent(getApplicationContext(),
				HelpActivity.class);
		startActivity(helpScreen);
	}
	
	/***************************************************
	 * This Listener method will validate the login 
	 * fields and will transition to the main menu screen
	 * if the login was successful
	 ***************************************************/

	private OnClickListener validateForm = new OnClickListener() {


		@Override
		public void onClick(View v) {
			if (mForm.validate()) {
				//create a loading dialog
				final ProgressDialog proDialog = new ProgressDialog(ctx);
				proDialog.setMessage("Logging In...");
			    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			    proDialog.show();
				User.checkLogin(email, password, new LogInCallback(){

					@Override
					public void done(ParseUser user, ParseException e) {
						User.redirect(user, LoginActivity.this, e);
						email.getEditableText().clear();
						email.requestFocus();
						password.getEditableText().clear();
						
						/***************************************************
						 * If the user does not exist as an Installation
						 * object in the Parse database, then they should
						 * be created and saved. This is required for sending
						 * push notifications via advanced targeting
						 **************************************************/
						
						ParseInstallation ins = ParseInstallation.getCurrentInstallation();
						
						if ((Boolean) ParseUser.getCurrentUser().getBoolean("isCarer")){
							PushService.setDefaultPushCallback(ctx, CarerMenuActivity.class);
							ParseInstallation.getCurrentInstallation().saveInBackground();
						}
						else{
							PushService.setDefaultPushCallback(ctx, APMenuActivity.class);
							ParseInstallation.getCurrentInstallation().saveInBackground();
						}
						
						String emailIns = ParseUser.getCurrentUser().getEmail();
						String nameIns = (String) ParseUser.getCurrentUser().get("name");
						
						ins.put("Email", emailIns);
						ins.put("Name", nameIns);
						ins.saveInBackground();
						
						proDialog.dismiss();
					}
					
				}); 
				
			}
		}
	};
}
