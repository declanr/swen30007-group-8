/**********************************************************
 * AlertScreenActivity
 * 
 * This class serves to show the user that the 'ALERT'
 * button was tapped. If the button was tapped by mistake
 * then it can be cancelled via the CANCEL button
 *********************************************************/

package unimelb.student.ugrad.swen30007g8;

import java.util.List;
import java.util.Map;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class AlertScreenActivity extends Activity {
	
	/***************************************************
	 * When the activity is created, the application
	 * will transition to the Alert screen
	 ***************************************************/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alert);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**********************************************************************
	 * Method to check if the CANCEL button was tapped.
	 * If the cancel button was tapped then it will send
	 * a message will be pushed to the server to cancel the alert and
	 * will change back to the previous screen
	 *********************************************************************/

	public void Cancel(View view) {
		
		/*************************************
		 * Setup variables for push
		 ************************************/
		 
		final ParseUser curUser = ParseUser.getCurrentUser();
		ParseInstallation ins = ParseInstallation.getCurrentInstallation();
		
		/**************************************************
		 * Get the users currently linked with the AP
		 * and send them a cancel alert message
		 *************************************************/
		
		LinkFunctions.getLinkedUsers(curUser, new FindCallback<ParseObject>(){

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				String name;
				List<Map<String, String>> list = LinkFunctions.getUsersDone(curUser, e, objects);
				
				for (int i = 0; i < list.size(); i++){
					name = list.get(i).get("name");
					ParseFunctions.sendPush(name, ParseFunctions.Comm.cancelAlert);
				}

			} 
	    	

	    });
		
		/**********************************************************
		 * Finish this activity and return to the main screen
		 *********************************************************/
		
		finish();
	}

}
