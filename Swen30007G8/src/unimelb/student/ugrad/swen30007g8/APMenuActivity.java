/**********************************************************
 * APMenuActivity
 * 
 * This class contains the different methods required
 * to start intents based on the button that an assisted
 * person taps on their main menu screen
 *********************************************************/

package unimelb.student.ugrad.swen30007g8;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import org.json.JSONException;
import org.json.JSONObject;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.PushService;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class APMenuActivity extends Activity {
	
	/***************************************************
	 * When the activity is created, this method serves 
	 * transition to the main menu screen for the assisted
	 * person to view. They will have different options
	 * compared to carers
	 ***************************************************/
	ParseUser curUser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_ap);

	}
	
	/***************************************************
	 * When the activity is resumed, the appropriate
	 * listeners will be attached to the buttons to
	 * send a push request to all linked users
	 ***************************************************/
	
	@Override
	protected void onResume() {
		
		
		super.onResume();	
		Button btnAlert = (Button) findViewById(R.id.btnAlertScreen);
		Button btnOk = (Button) findViewById(R.id.btnOK);
		
		btnAlert.setOnClickListener(alert);
		btnOk.setOnClickListener(sendOk);
		curUser = ParseUser.getCurrentUser();
		    
	}
	
	/***************************************************
	 * The Alert listener will fetch the users currently
	 * linked to the AP logged in and will send an alert
	 * push to all of them. It will also transition to
	 * the alert screen
	 ***************************************************/
	
	private OnClickListener alert = new OnClickListener() {
		
		
		@Override
		public void onClick(View v) {
			
			LinkFunctions.getLinkedUsers(curUser, new FindCallback<ParseObject>(){
				
				/**************************************************
				 * Get the users currently linked with the AP
				 * and send them an alert message
				 *************************************************/
				
				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					String name;
					List<Map<String, String>> list = LinkFunctions.getUsersDone(curUser, e, objects);
					
					for (int i = 0; i < list.size(); i++){
						name = list.get(i).get("name");
						ParseFunctions.sendPush(name, ParseFunctions.Comm.alert);
					}

				} 
		    	

		    });
			
			/***************************************
			 * Transition to the alert activity
			 **************************************/
		            
            createAlert(v);
	
		}
				
	};
	
	/************************************************************
	 * The sendOk listener will fetch the users currently
	 * linked to the AP logged in and will send an Ok message
	 * push to all of them. It will also send a message to the 
	 * currently logged in AP 
	 ***********************************************************/
	
	private OnClickListener sendOk = new OnClickListener(){
		
		@Override
		public void onClick(View v){
			
			/**************************************************
			 * Get the users currently linked with the AP
			 * and send them an I'm OK message
			 *************************************************/
			
			LinkFunctions.getLinkedUsers(curUser, new FindCallback<ParseObject>(){

				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					String name;
					List<Map<String, String>> list = LinkFunctions.getUsersDone(curUser, e, objects);
					
					for (int i = 0; i < list.size(); i++){
						name = list.get(i).get("name");
						ParseFunctions.sendPush(name, ParseFunctions.Comm.ok);
					}

				} 
		    	

		    });
			
			/**************************************************
			 * Send a push to the currently logged in AP to
			 * let them know that the message was sent
			 *************************************************/
			
			ParseFunctions.sendPush((String) curUser.get("name"),ParseFunctions.Comm.apOk);
			
		}
	};
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/***************************************************
	 * Method to check if the ALERT button was tapped.
	 * If the alert button was tapped then it will send
	 * a message to the server to send an alert to all
	 * carers on the carers list
	 ***************************************************/

	public void createAlert(View view) {
		Intent alertScreen = new Intent(getApplicationContext(),
				AlertScreenActivity.class);
		startActivity(alertScreen);
	}
	
	/**********************************************************
	 * Method to check if the MANAGE button was tapped.
	 * If the manage button was tapped then the application
	 * will initiate the APManageScreenActivity and the assisted
	 * person can interact with their carer's list
	 *********************************************************/
	
	public void getManageScreen(View view) {
		Intent manageScreen = new Intent(getApplicationContext(),
				APManageScreenActivity.class);
		startActivity(manageScreen);
	}
	
	/**********************************************************
	 * Method to check if the HELP button was tapped. If so 
	 * then we transition to the help screen activity
	 * to display the help text for the current user
	 *********************************************************/
	
	public void getHelpScreen(View view){
		Intent helpScreen = new Intent(getApplicationContext(),
				HelpActivity.class);
		startActivity(helpScreen);
	}
	
	/**********************************************************
	 * Method to log the user out of the application and to 
	 * return to the login screen
	 *********************************************************/

	public void logOut(View view) {
		ParseUser.logOut();
		finish();
	}

}
