/**********************************************************
 * CarerManageScreenActivity
 * 
 * This class serves to set up the manage screen for a carer
 * where they can view the current people they are caring
 * for and any pending links from other assisted persons
 *********************************************************/


package unimelb.student.ugrad.swen30007g8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class CarerManageScreenActivity extends Activity {

	/*************************************************************
	 * When the activity is created, this method serves 
	 * to transition to the manage screen for the carer to view.
	 * It also populates the lists of linked APs for the logged-in
	 * carer.
	 ************************************************************/
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage_carer);
		
		
		final ListView linkedview = (ListView) findViewById(R.id.linkedAP);
		final ListView pendingview = (ListView) findViewById(R.id.pendingAP);
		
		//create a loading dialog
		final ProgressDialog proDialog = new ProgressDialog(this);
		proDialog.setMessage("loading...");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	    
	    
	    final ParseUser user = ParseUser.getCurrentUser();
	 
	    /***************************************************
	     * Creates a list of currently linked users
	     ***************************************************/
	    LinkFunctions.getLinkedUsers(user, new FindCallback<ParseObject>(){

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				List<Map<String, String>> list = LinkFunctions.getUsersDone(user, e, objects);
				//add the values to the list
				SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), list, R.layout.list_item, new String[] {"name", "id"}, new int[] {R.id.user_name, R.id.user_id});
				linkedview.setAdapter(adapter);
				//no actions are tied to this linkview, remove the ontouch animations/actions
				linkedview.setClickable(false);
				linkedview.setFocusable(false);
				//remove the loading dialog
			    proDialog.dismiss();
			} 
	    	

	    });
	    /***************************************************
	     * Creates a list of currently Pending Users
	     * Adds onClick method to the list:
	     * * Pressing on a User's name will bring up an 
	     * * Accept/Reject dialog
	     ***************************************************/
	    LinkFunctions.getPendingUsers(user, new FindCallback<ParseObject>(){

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				List<Map<String, String>> list = LinkFunctions.getUsersDone(user, e, objects);
				
				//add the values to the list
				SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), list, R.layout.list_item, new String[] {"name", "id"}, new int[] {R.id.user_name, R.id.user_id});
				pendingview.setAdapter(adapter);

				pendingview.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub
						final TextView item = (TextView) view.findViewById(R.id.user_id);
						new AlertDialog.Builder(CarerManageScreenActivity.this)
						.setTitle("Confirm Request")
						.setMessage("Do you wish to accept or reject this request")
						.setIcon(android.R.drawable.ic_dialog_alert)
						.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
						    public void onClick(DialogInterface dialog, int whichButton) {
						    	LinkFunctions.acceptPending(item.getText().toString(), CarerManageScreenActivity.this);
						    }
						})
						.setNegativeButton(R.string.reject,  new DialogInterface.OnClickListener() {
						    public void onClick(DialogInterface dialog, int whichButton) {
							    LinkFunctions.rejectPending(item.getText().toString(), CarerManageScreenActivity.this);
							}
						}).show();
					}
					
				});
				
				//remove the loading dialog
			    proDialog.dismiss();
			} 
	    	

	    });
	    
	    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
