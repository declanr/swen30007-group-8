package unimelb.student.ugrad.swen30007g8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/***************************************************
 * Gets the ACCEPTED Linked Users for a ParseUser 
 * input, returns them into a FindCallBack input
 ***************************************************/
public class LinkFunctions {
	public static void getLinkedUsers(final ParseUser user, final FindCallback<ParseObject> callback) {
		/***************************************************
		 * Set up the query for the Linked Users table
		 * We only want links where:
		 * * The user is either the Carer or the AP
		 * * The link has been accepted
		 ***************************************************/
		ParseQuery<ParseObject> query = ParseQuery.getQuery("LinkedUsers");
		//if we're the carer then we want a list where the carer is us.
		if (user.getBoolean("isCarer") == true) {
			query.whereEqualTo("carerId", user.getObjectId());
		}
		else {
			query.whereEqualTo("apId", user.getObjectId());
		}
		
		query.whereEqualTo("isAccepted", true);
		
		
		query.findInBackground(new FindCallback<ParseObject>(){

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				
				callback.done(objects, e);
			}
			
		});
		
	}
	
	/***************************************************
	 * Gets the ACCEPTED Linked Users for a ParseUser 
	 * input, returns them into a FindCallBack input
	 ***************************************************/
	public static void getPendingUsers(final ParseUser user, final FindCallback<ParseObject> callback) {
		/***************************************************
		 * Set up the query for the Linked Users table
		 * We only want links where:
		 * * The user is either the Carer or the AP
		 * * The link has not been accepted
		 * * The link has not been rejected
		 ***************************************************/
		ParseQuery<ParseObject> query = ParseQuery.getQuery("LinkedUsers");
		//if we're the carer then we want a list where the carer is us.
		if (user.getBoolean("isCarer") == true) {
			query.whereEqualTo("carerId", user.getObjectId());
		}
		else {
			query.whereEqualTo("apId", user.getObjectId());
		}
		
		query.whereEqualTo("isAccepted", false);
		
		
		query.findInBackground(new FindCallback<ParseObject>(){

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				
				callback.done(objects, e);
			}
			
		});
		
	}
	
	/****************************************************************
	 * Use this inside the callback of the Get_____Users functions.
	 * Takes a ParseUser and the ParseException and List<ParseObject> 
	 * from Done in the callback
	 * Returns a List<Map<String, String>> of the format
	 * <User Name, User ObjectID>
	 ****************************************************************/
	public static List<Map<String, String>> getUsersDone(ParseUser user, ParseException e, List<ParseObject> objects) {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		Map<String, String> map;
		if (e == null) {
			//no error
			if (objects.size() != 0) {
				//names returned
				for (int i = 0; i<objects.size(); i++) {
					if (user.getBoolean("isCarer") == false) {
						map = new HashMap<String, String>();
						map.put("name", objects.get(i).getString("carerName"));
						map.put("id", objects.get(i).getObjectId());
						list.add(map);
					}
					else {
						map = new HashMap<String, String>();
						map.put("name", objects.get(i).getString("apName"));
						map.put("id", objects.get(i).getObjectId());
						list.add(map);
					}
				}
			}
			else {
				map = new HashMap<String, String>();
				map.put("name", "No Users Found");
				map.put("id", "-1");
				list.add(map);
			}
		}
		else {
			//error
			map = new HashMap<String, String>();
			map.put("name", "Couldn't Connect To Linked Users Database");
			map.put("id", "-1");
			list.add(map);
			System.out.println(e.getLocalizedMessage());
		}
		return list;
	}
	
	
	/***************************************************
	 * Setup function to get a Link by ObjectID since 
	 * it's a common requirement
	 * Takes a LinkID as a string, and a GetCallback 
	 * to return into 
	 ***************************************************/
	public static void retrieveLinkById(String linkId, final GetCallback<ParseObject> callback){
		ParseQuery<ParseObject> query = ParseQuery.getQuery("LinkedUsers");
	   	 
    	// Retrieve the object by id
    	query.getInBackground(linkId, new GetCallback<ParseObject>(){

			@Override
			public void done(ParseObject object, ParseException e) {
				callback.done(object, e);
				
			}
    		
    	});
	}
	
	/***************************************************
	 * Deletes the Link defined by an ObjectID
	 ***************************************************/
	public static void removeLink(String linkId, final Activity activity) {
		retrieveLinkById(linkId, new GetCallback<ParseObject>(){
			@Override
			public void done(ParseObject object, ParseException e) {
				if (e == null) {
					object.deleteInBackground(new DeleteCallback(){
						@Override
						public void done(ParseException e) {
							clickDone(activity, "Carer Removed!");
						}					
					});
				}
				
			}
			
		});
	}
	
	/***************************************************
	 * Accepts the pending Link defined by an ObjectID
	 ***************************************************/
	public static void acceptPending(String linkId, final Activity activity){
		retrieveLinkById(linkId, new GetCallback<ParseObject>(){
			@Override
			public void done(ParseObject object, ParseException e) {
				if (e == null) {
					object.put("isAccepted", true);
					object.saveInBackground(new SaveCallback(){

						@Override
						public void done(ParseException e) {
							clickDone(activity, "Accepted request");							
						}
					});
				}
				else {
					Toast.makeText(activity, e.toString(), Toast.LENGTH_SHORT).show();
				}
			}
    	});
	}
	
	/***************************************************
	 * Deletes the pending Link defined by an ObjectID
	 ***************************************************/
	public static void rejectPending(String linkId, final Activity activity){
		retrieveLinkById(linkId, new GetCallback<ParseObject>(){
			@Override
			public void done(ParseObject object, ParseException e) {
				if (e == null) {
		    	      object.deleteInBackground(new DeleteCallback(){

							@Override
							public void done(ParseException e) {
								clickDone(activity, "Rejected Request");
								
							}
			    	    	  
			    	      });
		    	    }
		    	    else {
		    	    	Toast.makeText(activity, e.toString(), Toast.LENGTH_SHORT).show();
		    	    }
			}
			
		});
	}
	
	/****************************************************
	 * Used at the end of a choice dialog.
	 * Toasts the string to the screen and then refreshes
	 * the activity without an animation
	 ****************************************************/
	public static void clickDone(Activity activity, String toast){
		Toast.makeText(activity.getApplicationContext(), toast, Toast.LENGTH_SHORT).show();
	    Intent i = activity.getIntent();
	    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
	    activity.finish();
	    activity.overridePendingTransition(0, 0);
	    activity.startActivity(i);
	}

	/***************************************************
	 * Creates a link between the logged in ParseUser 
	 * and the user associated with the input email.
	 * Needs to be called after checking that no link 
	 * exists
	 ***************************************************/
	public static void addLink(String email, final Activity activity, final ProgressDialog proDialog){
		ParseQuery<ParseUser> carerquery = ParseUser.getQuery();
		carerquery.whereEqualTo("email", email);
		carerquery.whereEqualTo("isCarer", true);
		carerquery.findInBackground(new FindCallback<ParseUser>() {

			public void done(List<ParseUser> objects, ParseException e) {
				if (e == null) {
					
					//no error - success
					ParseObject newlink = new ParseObject("LinkedUsers");
					newlink.put("apId", ParseUser.getCurrentUser().getObjectId());
					newlink.put("apName", ParseUser.getCurrentUser().get("name"));
					newlink.put("carerId", objects.get(0).getObjectId());
					newlink.put("carerName", objects.get(0).get("name"));
					newlink.put("isAccepted", false);
					newlink.put("isRejected", false);
					newlink.saveInBackground(new SaveCallback(){

						@Override
						public void done(ParseException arg0) {
							if (arg0 == null) {
								//link saved correctly
								Toast.makeText(activity.getApplicationContext(), "Request Sent", Toast.LENGTH_SHORT).show();
								activity.finish();
								activity.overridePendingTransition(0, 0);
							}
							else {
								Toast.makeText(activity.getApplicationContext(), "Can't make link: "+arg0.toString(), Toast.LENGTH_SHORT).show();
							}
						}
						
					});
				}
				else {
					Toast.makeText(activity.getApplicationContext(), "Can't find carer: "+e.toString(), Toast.LENGTH_SHORT).show();
				}
				proDialog.dismiss();
			}
		});
		
	}
}

