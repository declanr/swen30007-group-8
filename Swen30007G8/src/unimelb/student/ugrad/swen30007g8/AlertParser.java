package unimelb.student.ugrad.swen30007g8;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

public class AlertParser {
	
	/***************************************************
	 * Class that stores the data of alerts parsed from
	 * the XML file
	 ***************************************************/
	public static class Alert
	{
		public final String ap;
		public final boolean cancelled;
		
		private Alert(String ap, boolean cancelled)
		{
			this.ap = ap;
			this.cancelled = cancelled;
		}
	}
	
	//Namespaces aren't used
	private static final String ns = null;
	
	/***************************************************
	 * The method to be called on XML files with alerts
	 * from outside. Takes an InputStream, in, and
	 * parses it through an XmlPullParser, creating a
	 * list of Alerts which stores all relevant data
	 ***************************************************/
	public List<Alert> parse(InputStream in) throws XmlPullParserException, IOException
	{
		try
		{
			//Sets up the XmlPullParser
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			//Begins reading the XML file
			return readAlerts(parser);
		}
		finally
		{
			//Closes the input given
			in.close();
		}
		
	}

	/***************************************************
	 * This method steps through the XML file using
	 * the supplied parser, creating a list of Alerts
	 * as it goes, and skipping all other tags
	 ***************************************************/
	private List<Alert> readAlerts(XmlPullParser parser) throws XmlPullParserException, IOException
	{
		List<Alert> alerts = new ArrayList<Alert>();
		
		while(parser.next() != XmlPullParser.END_DOCUMENT)
		{
			//Skips until the parser finds the next start tag in the XML file
			if (parser.getEventType() != XmlPullParser.START_TAG)
			{
				continue;
			}
			
			//Checks whether the current tag is for the alert structure, parsing it if it is
			//and skipping it if it doesn't
			String name = parser.getName();
			if (name.equals("alert"))
			{
				alerts.add(readAlert(parser));
			}
			else
			{
				skip(parser);
			}
		}
		
		return alerts;
	}

	/***************************************************
	 * Method that skips through the current tag and
	 * all children tags
	 ***************************************************/
	private void skip(XmlPullParser parser) throws XmlPullParserException, IOException
	{
		//Makes sure the current tag is the start of the structure
		if (parser.getEventType() != XmlPullParser.START_TAG)
		{
			throw new IllegalStateException();
		}
		
		//Works through each tag until the original start tag's end tag has been reached
		int depth = 1;
		while (depth!= 0) {
			switch (parser.next())
			{
			case XmlPullParser.END_TAG:
				depth --;
				break;
			case XmlPullParser.START_TAG:
				depth ++;
				break;
			}
		}
	}

	/***************************************************
	 * This method reads through a single Alert
	 * structure in the XML and returns an Alert object
	 * containing the relevant data
	 ***************************************************/
	private Alert readAlert(XmlPullParser parser) throws XmlPullParserException, IOException {
		//Checks that the parser is currently at an alert tag
		parser.require(XmlPullParser.START_TAG, ns, "alert");
		//The assisted person's name and whether the alert is cancelled or not
		String ap = null;
		boolean cancelled = false;
		
		//Goes through the children of the Alert tag, and interprets assistedperson and type tags
		while (parser.next() != XmlPullParser.END_TAG)
		{
			if (parser.getEventType() != XmlPullParser.START_TAG)
			{
				continue;
			}
			String name = parser.getName();
			if (name.equals("assistedperson"))
			{
				ap = readAssistedPerson(parser);
			}
			else if (name.equals("type"))
			{
				cancelled = readType(parser);
			}
			else
			{
				skip(parser);
			}
		}
		return new Alert(ap, cancelled);
	}

	/***************************************************
	 * This method gets whether the alert has been
	 * cancelled or not from the type tag
	 ***************************************************/
	private boolean readType(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "type");
		String type = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "type");
		if (type.equals("cancel"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/***************************************************
	 * This method returns the text content of tag
	 * not associated with any variable
	 ***************************************************/
	private String readText(XmlPullParser parser) throws XmlPullParserException, IOException {
		String result = "";
		if (parser.next() == XmlPullParser.TEXT)
		{
			result = parser.getText();
			parser.nextTag();
		}
		return result;
	}

	/***************************************************
	 * This method gets the assisted person associated
	 * with this alert
	 ***************************************************/
	private String readAssistedPerson(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "assistedperson");
		String ap = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "assistedperson");
		return ap;
	}

}
