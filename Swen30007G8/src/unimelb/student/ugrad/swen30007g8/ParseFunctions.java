/**********************************************************
 * ParseFunctions
 * 
 * This class serves to house all of the Parse functions
 * used in this application
 *********************************************************/

package unimelb.student.ugrad.swen30007g8;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.View;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;



public class ParseFunctions {
	
	/***********************************
	* Function to connect to the Parse
	* service using our keys
	***********************************/

	public static void parseInit(Context context) {
		Parse.initialize(context, "sPdhRmqEeAl8MqWyErlGoOG2OGyiz56oLNKIvEq2",
				"8id7vyZ6aLwVhAkYt4rNBIE7xw6wtOZHzkeb1gss");
	}
	
	/***********************************
	* The register function will create
	* a new user on our Parse database
	* and will fill in the appropriate
	* fields
	***********************************/
	
	public static void register(String name, String email, String password, boolean isCarer, final SignUpCallback callback) {
		ParseUser newUser = new ParseUser();
		newUser.setUsername(email);
		newUser.setEmail(email);
		newUser.setPassword(password);
		newUser.put("name", name);
		newUser.put("isCarer", isCarer);
		if (!newUser.getBoolean("isCarer")){
			newUser.put("isOk", false);
		}
		
		newUser.signUpInBackground(new SignUpCallback() {
			  public void done(ParseException e) {
				  callback.done(e);
			  }
		});
	}
	
	/***********************************
	* Enum required for communications
	***********************************/
	
	public enum Comm{
		alert, cancelAlert, ok, notSentOk, apOk
	}
	
	/***********************************************************************
	* This function will send a push notification using advanced targeting
	* to a user and the enum Commtype will select what type of message to
	* send
	***********************************************************************/
	
	public static void sendPush(String name, Comm type){
				
				/***********************************
				* Create our Installation push query
				***********************************/
				ParseInstallation ins = ParseInstallation.getCurrentInstallation();
		
				ParseQuery pushQuery = ParseInstallation.getQuery();
				pushQuery.whereEqualTo("Name", name);
				
				String ap = ParseUser.getCurrentUser().getEmail();
				JSONObject data = new JSONObject();
				
				/***********************************
				* Depending on the type of message
				* intended to be sent, the JSON
				* data object will contain different
				* data values for their respective
				* fields
				***********************************/
				
				switch(type){
					case alert:
						try {
							data.put("alert", ap + " has sent you an alert!" );
							data.put("title", "Alert Message");
							data.put("action", "Display");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						};
						break;
						
					case cancelAlert: 
						try {
							data.put("alert", ap + " has cancelled the alert!" );
							data.put("title", "Alert Message");
							data.put("action", "Display");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						};
					
						break;
						
					case ok:
						try {
							data.put("alert", ap + " is OK for today!" );
							data.put("title", "OK Message");
							data.put("action", "Display");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						};
						
						break;
						
					case notSentOk:
						try {
							data.put("alert", ap + " has not sent an I'm OK message today!" );
							data.put("title", "Not OK Message");
							data.put("action", "Display");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						};
						
						break;
						
					case apOk:
						try {
							data.put("alert", "Your message has been sent!" );
							data.put("title", "OK Message");
							data.put("action", "Display");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						};
						
						break;
						
				}
			


				/***********************************
				* Push using push query and advanced
				* targeting
				***********************************/
	
	            ParsePush push = new ParsePush();
	            push.setQuery(pushQuery);
	            push.setData(data);
	            push.sendInBackground();		
		
	}

}
