/**********************************************************
 * RegisterActivity
 * 
 * This class serves to set up the registration screen for 
 * the user to interact with. It will also check that the
 * user has entered information into the fields and will 
 * register the user with the server according to the 
 * information provided
 *********************************************************/

package unimelb.student.ugrad.swen30007g8;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.throrinstudio.android.common.libs.validator.Form;
import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validator.EmailValidator;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;

public class RegisterActivity extends Activity {
	
	/***************************************************
	 * Variables declared for validation and for checking
	 * fields within the Radio Group
	 ***************************************************/
	
	private Form mForm;
	private EditText email;
	private EditText name;
	private Validate emailField;
	private Validate nameField;
	private Validate passField;
	private EmailValidator emailVal;
	private EditText password1;
	private EditText password2;
	private RadioGroup isCarer;
	
	/********************************************************
	 * When the activity is created, this method serves 
	 * to set up the registration fields for the user with 
	 * validation involved.
	 *******************************************************/
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		this.mForm = new Form();

		email = (EditText) findViewById(R.id.email);
		name = (EditText) findViewById(R.id.name);
		password1 = (EditText) findViewById(R.id.pass);
		password2 = (EditText) findViewById(R.id.passconfirm);
		isCarer = (RadioGroup) findViewById(R.id.isCarer);
		
		emailField = new Validate(email);
		nameField = new Validate(name);
		passField = new Validate(password1);
		
		emailVal = new EmailValidator(this);

		emailField.addValidator(new NotEmptyValidator(this));
		emailField.addValidator(emailVal);
		nameField.addValidator(new NotEmptyValidator(this));
		passField.addValidator(new NotEmptyValidator(this));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/***************************************************
	 * When the activity is created, this method serves 
	 * to set up the registration fields for the user with 
	 * validation involved. A listener is also bound
	 * to the registration button on resume
	 ***************************************************/
	
	@Override
	protected void onResume() {
		super.onResume();

		mForm.addValidates(emailField);
		mForm.addValidates(nameField);
		mForm.addValidates(passField);
		Button reg = (Button) findViewById(R.id.btn_register);
		reg.setOnClickListener(validateForm);
	}
	
	/***********************************************************************
	 * This Listener method will validate the registration fields 
	 * and will contact the server. If the registration was successful
	 * then the application will transition to the login screen. If passwords
	 * entered didn't match or registration was unsuccessful then the user 
	 * will be notified by a pop up
	 **********************************************************************/
	
	private OnClickListener validateForm = new OnClickListener() {

		// To be completed once the server communication has been implemented
		@Override
		public void onClick(View v) {
			if (mForm.validate()) {
				if (email.getText().toString().matches("[a-zA-Z0-9.@]*")){
					if (checkPassword(password1, password2)) {
						boolean carer = false;
						if (isCarer.getCheckedRadioButtonId() == R.id.rdbtnCarer) {
							carer = true;
						}
						ParseFunctions.register(name.getText().toString(), email.getText().toString(), password1.getText().toString(), carer, new SignUpCallback() {
							
							@Override
							public void done(ParseException arg0) {
								if (arg0 == null){
									Toast.makeText(getApplicationContext(), "Registration Successful!", Toast.LENGTH_LONG).show();
									finish();
								}
								else {
									Toast.makeText(getApplicationContext(), arg0.toString(), Toast.LENGTH_LONG).show();
								}
							}
						});
					}
					else{
						Toast.makeText(getApplicationContext(), "Passwords didn't match, please enter again", Toast.LENGTH_SHORT).show();
					}
				}
				else {
					Toast.makeText(getApplicationContext(), "Please remove irregular characters from your email address", Toast.LENGTH_SHORT).show();					
				}
			}
		}
	};
	
	public void getMainMenu(View view) {
		finish();
	}
	
	
	/***************************************************
	 * This method checks that the passwords entered
	 * match using string comparison
	 ***************************************************/
	
	public boolean checkPassword(EditText pass1, EditText pass2){
	
		if (pass1.getText().toString().equals(pass2.getText().toString())){
			return true;
		}
		
		return false;
	}
	
}
