package unimelb.student.ugrad.swen30007g8;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import unimelb.student.ugrad.swen30007g8.AlertParser.Alert;
import android.util.Xml;

public class CommParser {
	
	/***************************************************
	 * Class that stores the data of other
	 * communications parsed from the XML file
	 ***************************************************/
	public static class OtherComm
	{
		public final String ap;
		public final boolean ok;
		
		private OtherComm(String ap, boolean ok)
		{
			this.ap = ap;
			this.ok = ok;
		}
	}
		
		//Namespaces are unused
		private static final String ns = null;
		
		/***************************************************
		 * The method to be called on XML files with other
		 * communications from outside. Takes an InputStream,
		 * in, and parses it through an XmlPullParser,
		 * creating a list of OtherComm which stores all
		 * relevant data
		 ***************************************************/
		public List<OtherComm> parse(InputStream in) throws XmlPullParserException, IOException
		{
			try
			{
				//Sets up the XmlPullParser
				XmlPullParser parser = Xml.newPullParser();
				parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
				parser.setInput(in, null);
				//Begins reading the XML file
				return readComms(parser);
			}
			finally
			{
				//Closes the input given
				in.close();
			}
			
		}
		
		/***************************************************
		 * This method steps through the XML file using
		 * the supplied parser, creating a list of OtherComm
		 * as it goes, and skipping all other tags
		 ***************************************************/
		private List<OtherComm> readComms(XmlPullParser parser) throws XmlPullParserException, IOException
		{
			List<OtherComm> comms = new ArrayList<OtherComm>();
			
			while(parser.next() != XmlPullParser.END_DOCUMENT)
			{
				//Skips until the parser finds the next start tag in the XML file
				if (parser.getEventType() != XmlPullParser.START_TAG)
				{
					continue;
				}
				//Checks whether the current tag is for the comm structure, parsing it if it is
				//and skipping it if it doesn't
				String name = parser.getName();
				if (name.equals("comm"))
				{
					comms.add(readComm(parser));
				}
				else
				{
					skip(parser);
				}
			}
			
			return comms;
		}
		
		/***************************************************
		 * Method that skips through the current tag and
		 * all children tags
		 ***************************************************/
		private void skip(XmlPullParser parser) throws XmlPullParserException, IOException
		{
			//Makes sure the current tag is the start of the structure
			if (parser.getEventType() != XmlPullParser.START_TAG)
			{
				throw new IllegalStateException();
			}
			//Works through each tag until the original start tag's end tag has been reached
			int depth = 1;
			while (depth!= 0) {
				switch (parser.next())
				{
				case XmlPullParser.END_TAG:
					depth --;
					break;
				case XmlPullParser.START_TAG:
					depth ++;
					break;
				}
			}
		}
	
		/***************************************************
		 * This method reads through a single Comm
		 * structure in the XML and returns an OtherComm
		 * object containing the relevant data
		 ***************************************************/
		private OtherComm readComm(XmlPullParser parser) throws XmlPullParserException, IOException {
			//Checks that the parser is currently at a comm tag
			parser.require(XmlPullParser.START_TAG, ns, "comm");
			//The assisted person's name and whether the communication is that they're okay or not
			String ap = null;
			boolean ok = false;
			
			//Goes through the children of the Comm tag, and interprets assistedperson and type tags
			while (parser.next() != XmlPullParser.END_TAG)
			{
				if (parser.getEventType() != XmlPullParser.START_TAG)
				{
					continue;
				}
				String name = parser.getName();
				if (name.equals("assistedperson"))
				{
					ap = readAssistedPerson(parser);
				}
				else if (name.equals("type"))
				{
					ok = readType(parser);
				}
				else
				{
					skip(parser);
				}
			}
			return new OtherComm(ap, ok);
		}
		
		/***************************************************
		 * This method gets whether the communication is
		 * that the assisted person is okay, or that they
		 * still have not sent that message
		 ***************************************************/
		private boolean readType(XmlPullParser parser) throws XmlPullParserException, IOException {
			parser.require(XmlPullParser.START_TAG, ns, "type");
			String type = readText(parser);
			parser.require(XmlPullParser.END_TAG, ns, "type");
			if (type.equals("OK"))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		/***************************************************
		 * This method returns the text content of tag
		 * not associated with any variable
		 ***************************************************/
		private String readText(XmlPullParser parser) throws XmlPullParserException, IOException {
			String result = "";
			if (parser.next() == XmlPullParser.TEXT)
			{
				result = parser.getText();
				parser.nextTag();
			}
			return result;
		}

		/***************************************************
		 * This method gets the assisted person associated
		 * with this communication
		 ***************************************************/
		private String readAssistedPerson(XmlPullParser parser) throws XmlPullParserException, IOException {
			parser.require(XmlPullParser.START_TAG, ns, "assistedperson");
			String ap = readText(parser);
			parser.require(XmlPullParser.END_TAG, ns, "assistedperson");
			return ap;
		}

}
