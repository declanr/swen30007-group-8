/**********************************************************
 * CarerMenuActivity
 * 
 * This class contains the different methods required
 * to start intents based on the button that a carer
 * taps on their main menu screen
 *********************************************************/

package unimelb.student.ugrad.swen30007g8;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import unimelb.student.ugrad.swen30007g8.AlertParser.Alert;
import unimelb.student.ugrad.swen30007g8.CommParser.OtherComm;

import com.parse.ParseUser;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class CarerMenuActivity extends Activity {
	
	/***************************************************
	 * When the activity is created, this method serves 
	 * transition to the main menu screen for the carer
	 * to view. They will have different options compared
	 * to assisted persons
	 ***************************************************/
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ParseFunctions.parseInit(this);
		setContentView(R.layout.activity_main_carer);
		
		try{
			//Checks that alerts.xml exists, and if not, creates the file
			File file  = new File(this.getFilesDir(), "alerts.xml");;
			FileWriter out = new FileWriter(file);
			
			if (!file.exists())
			{
				file.createNewFile();
				out.write("<?xml version=\"1.0\" encoding =\"utf-8\"?>\n");
			}
			
			out.close();
			
			//Checks that otherComm.xml exists, and if not, creates the file
			file  =  new File(this.getFilesDir(), "otherComm.xml");
			out = new FileWriter(file);
			if (!file.exists())
			{
				file.createNewFile();
				out.write("<?xml version=\"1.0\" encoding =\"utf-8\"?>\n");
			}
		}
		catch (IOException e)
		{
			Log.d("Exception", e.toString());
		}
		
		//Loads all stored Alerts and Communications into their respective lists on the screen
		ListViewLoaderTaskAlerts listViewLoaderTaskAlerts = new ListViewLoaderTaskAlerts();
		ListViewLoaderTaskOther listViewLoaderTaskOther = new ListViewLoaderTaskOther();
		
		listViewLoaderTaskAlerts.execute("alerts.xml");
		listViewLoaderTaskOther.execute("otherComm.xml");

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/****************************************************************
	 * Method to check if the View Requests button was tapped.
	 * If the View Requests button was tapped then the application
	 * will initiate the CarerManageScreenActivity and the carer
	 * can view who is currently listed on their list and who would
	 * like to link with them
	 ***************************************************************/

	public void viewRequests(View view) {
		Intent manageScreen = new Intent(getApplicationContext(),
				CarerManageScreenActivity.class);
		startActivity(manageScreen);
	}
	
	/****************************************************************
	 * Method to check if the Help button was tapped.
	 * If the Help button was tapped then the application
	 * will initiate the HelpActivity
	 ***************************************************************/
	public void getHelpScreen(View view){
		Intent helpScreen = new Intent(getApplicationContext(),
				HelpActivity.class);
		startActivity(helpScreen);
	}
	
	/****************************************************************
	 * Method to check if the Log Out button was tapped.
	 * If the Log Out button was tapped then the application
	 * will log the current user out of the application
	 ***************************************************************/
	public void logOut(View view) {
		ParseUser.logOut();
		finish();
	}
	
	
	/****************************************************************
	 * Class that handles the displaying of Alerts in a list on
	 * the main menu page for the carer, parsing input from an XML
	 * file
	 ***************************************************************/
	private class ListViewLoaderTaskAlerts extends AsyncTask<String, Void, SimpleAdapter>
	{
		/****************************************************************
		 * Method that creates a SimpleAdapter using data parsed from
		 * an XML file containing data about alerts to be used to create
		 * a ListView
		 ***************************************************************/
		@Override
		protected SimpleAdapter doInBackground(String... xmlData)
		{
			//Reads in data from specified XML file and parses it into a list of
			//alerts
			AlertParser alertParser = new AlertParser();
			File file = new File(getApplicationContext().getFilesDir(), xmlData[0]);
			List<Alert> alerts = null;
			try {
				InputStream in = new FileInputStream(file);
				alerts = alertParser.parse(in) ;
			} 
			catch (Exception e) 
			{
				Log.d("Exception", e.toString());
				alerts = new ArrayList<Alert>();
			} 
			
			//The keys of the hash
			String[] from = {"ap","isAlert"};
			
			// Prepare the list of all records into a HashMap
	        List<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();
	        for(int i=0; i<alerts.size(); i++)
	        {
	            HashMap<String, String> map = new HashMap<String, String>();
	            map.put("ap", alerts.get(i).ap);
	            if (alerts.get(i).cancelled)
	            	map.put("isAlert", "Alert Cancelled");
	            else
	            	map.put("isAlert", "Alert Sent");
	            data.add(map);
	        }
			
	        //The format of the data in the ListView
			int[] to = {android.R.id.text1, android.R.id.text2};
			
			//The adapter that will be used to setup the ListView
			SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), data, android.R.layout.simple_list_item_2, from, to );
			
			return adapter;
		}
		
		/****************************************************************
		 * Method that uses the adapter to generate a list
		 ***************************************************************/
		@Override
		protected void onPostExecute(SimpleAdapter adapter)
		{
			//Getting a reference to list view for alerts of activity_main_carer.xml
			ListView listview = (ListView) findViewById(R.id.alerts);
			
			//Creates the list formatted by the adapter
			listview.setAdapter(adapter);
		}
	}
	
	/****************************************************************
	 * Class that handles the displaying of Other Communications in a 
	 * list on the main menu page for the carer, parsing input 
	 * from an XML file
	 ***************************************************************/
	private class ListViewLoaderTaskOther extends AsyncTask<String, Void, SimpleAdapter>
	{
		/****************************************************************
		 * Method that creates a SimpleAdapter using data parsed from
		 * an XML file containing data about other communications to
		 * be used to create a ListView
		 ***************************************************************/
		@Override
		protected SimpleAdapter doInBackground(String... xmlData)
		{
			//Reads in data from specified XML file and parses it into a list of
			//other communications
			CommParser commParser = new CommParser();
			File file = new File(getApplicationContext().getFilesDir(), xmlData[0]);
			List<OtherComm> comms = null;
			try {
				InputStream in = new FileInputStream(file);
				comms = commParser.parse(in) ;
			} 
			catch (Exception e) 
			{
				Log.d("Exception", e.toString());
				comms = new ArrayList<OtherComm>();
			} 
			
			//The keys of the hash
			String[] from = {"ap","isOK"};
			
			// prepare the list of all records into a HashMap
	        List<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();
	        for(int i=0; i<comms.size(); i++)
	        {
	            HashMap<String, String> map = new HashMap<String, String>();
	            map.put("ap", comms.get(i).ap);
	            if (comms.get(i).ok)
	            	map.put("isOK", "Has sent an OK message today");
	            else
	            	map.put("isOK", "Has not sent an OK message today");
	            data.add(map);
	        }
			
	        //The format of the data in the list view
			int[] to = {android.R.id.text1, android.R.id.text2};
			
			//The adapter that will be used to setup the ListView
			SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), data, android.R.layout.simple_list_item_2, from, to );
			
			return adapter;
		}
		
		@Override
		protected void onPostExecute(SimpleAdapter adapter)
		{
			//Getting a reference to list view for alerts of activity_main_carer.xml
			ListView listview = (ListView) findViewById(R.id.comms);
			
			//Creates the list formatted by the adapter
			listview.setAdapter(adapter);
		}
	}
	
	public void refresh(View view){
		LinkFunctions.clickDone(this, "Refreshed");
	}
}