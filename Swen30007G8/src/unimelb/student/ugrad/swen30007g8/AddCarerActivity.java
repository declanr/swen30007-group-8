/************************************************
 * AddCarerActivity
 * 
 * This activity is for an assisted person to 
 * add a carer using their email
 ***********************************************/

package unimelb.student.ugrad.swen30007g8;

import java.util.List;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.throrinstudio.android.common.libs.validator.Form;
import com.throrinstudio.android.common.libs.validator.Validate;
import com.throrinstudio.android.common.libs.validator.validator.EmailValidator;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.PushService;


public class AddCarerActivity extends Activity {
	
	/************************************************
	 * Setup the variables required for validation
	 ***********************************************/
	
	private Form mForm;
	private EditText email;
	private Validate emailField;
	private EmailValidator emailVal;
	
	
	/***************************************************
	 * When the activity is created, the application
	 * will transition to the Add Carer screen for the
	 * assisted person
	 ***************************************************/
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_carer);
		
		ParseFunctions.parseInit(getApplicationContext());
		
		/***************************************************
		 * Setup the form 
		 ***************************************************/
		
		this.mForm = new Form();

		email = (EditText) findViewById(R.id.carerEmail);
		//allow people to press enter to trigger the button
		
		/***************************************************
		 * If Enter is pressed then the application will
		 * also validate the form and send a request to the
		 * carer
		 ***************************************************/
		
		email.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keycode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_DOWN)
		        {
		            switch (keycode)
		            {
		                case KeyEvent.KEYCODE_DPAD_CENTER:
		                case KeyEvent.KEYCODE_ENTER:
		                    validateForm.onClick(v);
		                    return true;
		                default:
		                    break;
		            }
		        }
		        return false;
			}
			
		});
		
		/***************************************************
		 * Setup the form for validation
		 ***************************************************/
		
		emailField = new Validate(email);

		emailVal = new EmailValidator(this);

		emailField.addValidator(new NotEmptyValidator(this));
		emailField.addValidator(emailVal);

	}
	
	/***************************************************
	 * When the activity resumes we attach a listener
	 * to the button for adding a carer
	 ***************************************************/
	
	@Override
	protected void onResume() {
		super.onResume();

		mForm.addValidates(emailField);
		Button add = (Button) findViewById(R.id.btn_add);
		add.setOnClickListener(validateForm);
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/***************************************************
	 * Action for adding a link, called when Add Carers 
	 * button is pressed.
	 * Relies on the validator to make sure the form is 
	 * valid.
	 ***************************************************/
	private OnClickListener validateForm = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (mForm.validate()) {
				final ProgressDialog proDialog = new ProgressDialog(AddCarerActivity.this);
				proDialog.setMessage("loading...");
				proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				proDialog.show();
				
				/*****************************************
				 * Logic to make sure there isn't already
				 * a link between the users
				 *****************************************/
				LinkFunctions.getLinkedUsers(ParseUser.getCurrentUser(), new FindCallback<ParseObject>(){
					@Override
					public void done(List<ParseObject> arg0, ParseException arg1) {
						if (arg0.size() == 0) {
							LinkFunctions.getPendingUsers(ParseUser.getCurrentUser(), new FindCallback<ParseObject>() {
								@Override
								public void done(List<ParseObject> objects,
									ParseException e) {
									if (objects.size() == 0){
										LinkFunctions.addLink(email.getText().toString(), AddCarerActivity.this, proDialog);
									}
									else {
										Toast.makeText(AddCarerActivity.this, "Already a pending Link for these users", Toast.LENGTH_SHORT).show();
										proDialog.dismiss();
									}
								}
							});
						}
						else {
							Toast.makeText(AddCarerActivity.this, "Already a Link for these users", Toast.LENGTH_SHORT).show();
							proDialog.dismiss();
						}
					}
					
				});
			}
			
		}
		
	};



}
