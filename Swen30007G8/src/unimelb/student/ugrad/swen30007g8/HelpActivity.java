/************************************************
 * HelpActivity
 * 
 * This class is for any user to view the
 * Help screen. The activity reads from a file
 * to display to the user at runtime
 ***********************************************/

package unimelb.student.ugrad.swen30007g8;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;


public class HelpActivity extends Activity{
	
	/***************************************************
	 * When the activity is created, the application
	 * will transition to the Help Screen and the help
	 * text file will be opened for reading
	 ***************************************************/
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		/************************************************
		 * Set the content view for the Help Screen
		 ***********************************************/
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);
		
		/************************************************
		 * Use the asset manager to retrieve the help
		 * file from the assets folder and open
		 * the file for reading
		 ***********************************************/
		
		AssetManager am = getAssets();
		
		TextView helpText = (TextView)findViewById(R.id.helpText);

		InputStream helpFile = null;
		
		try {
			helpFile = am.open("Help");
			display(helpFile, helpText);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try{
				if (helpFile != null){
					helpFile.close();
				}
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		
		

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/***************************************************
	 * Finish the activity once the 'Go Back' button
	 * is pressed
	 ***************************************************/
	
	
	public void goBack(View view){
		this.finish();
	}
	
	/********************************************************
	 * This method will display the contents of the text file 
	 * on the screen and close the file
	 *******************************************************/
	
	public void display(InputStream stream, TextView helpText){
		
		/********************************************************
		 * Use the BufferedReader class to read the file line
		 * by line and append it to the appropriate TextView
		 *******************************************************/
		
		BufferedReader reader = null;
		String line;
		try {
			reader = new BufferedReader(new InputStreamReader(stream));
			while((line = reader.readLine()) != null){
				helpText.append(line);
				helpText.append("\n");
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/********************************************************
		 * Close the file once we are done with the processing
		 *******************************************************/
		
		finally {
		    try {
		        if (reader != null) {
		            reader.close();
		            stream.close();
		        }
		    } catch (IOException e) {
		    	e.printStackTrace();
		    }
	    }
   }
}
