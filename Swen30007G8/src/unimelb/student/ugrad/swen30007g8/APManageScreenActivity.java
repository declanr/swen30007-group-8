/**********************************************************
 * APManageScreenActivity
 * 
 * This class serves to set up the manage screen for an
 * assisted person where they can add or remove carers
 * from their list and can view who is currently listed
 * as their carer
 *********************************************************/

package unimelb.student.ugrad.swen30007g8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class APManageScreenActivity extends Activity {

	/*******************************************************
	 * When the activity is created, this method serves 
	 * to transition to the manage screen for the assisted
	 * person to view
	 ******************************************************/
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage_ap);
		
		final ListView linkedview = (ListView) findViewById(R.id.linkedCarers);
		final ListView pendingview = (ListView) findViewById(R.id.pendingCarers);
		
		//create a loading dialog
		final ProgressDialog proDialog = new ProgressDialog(this);
		proDialog.setMessage("loading...");
		proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		proDialog.show();
			    
			    
		final ParseUser user = ParseUser.getCurrentUser();
			 
		/***************************************************
		 * Create list of Linked Users	    
		 ****************************************************/	    
		LinkFunctions.getLinkedUsers(user, new FindCallback<ParseObject>(){

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				List<Map<String, String>> list = LinkFunctions.getUsersDone(user, e, objects);
				//add the values to the list
				SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), list, R.layout.list_item, new String[] {"name", "id"}, new int[] {R.id.user_name, R.id.user_id});
				linkedview.setAdapter(adapter);
				linkedview.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub
						final TextView item = (TextView) view.findViewById(R.id.user_id);
						new AlertDialog.Builder(APManageScreenActivity.this)
						.setTitle("Remove Carer?")
						.setMessage("Do you really want to remove this person as a carer?")
						.setIcon(android.R.drawable.ic_dialog_alert)
						.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
						    public void onClick(DialogInterface dialog, int whichButton) {
						    	LinkFunctions.removeLink(item.getText().toString(), APManageScreenActivity.this);
						    }})
						 .setNegativeButton(android.R.string.no,  new DialogInterface.OnClickListener() {
							    public void onClick(DialogInterface dialog, int whichButton) {
							    	dialog.dismiss();
							    }
						 }).show();
					}
					
				});
				//remove the loading dialog
			    proDialog.dismiss();
			}
		});
		
		/***************************************************
		 * Create list of Pending Users.
		 * Created by a separate function since this needs 
		 * to be re-called when a user is added.
		 ***************************************************/
		refreshPending(APManageScreenActivity.this);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		refreshPending(APManageScreenActivity.this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/***************************************************
	 * Called when the Add User button is pressed.
	 * Transitions to the Add User Activity
	 ***************************************************/
	public void getAddScreen(View view){
		Intent addScreen = new Intent(getApplicationContext(),
				AddCarerActivity.class);
		startActivity(addScreen);
	}
	
	/***************************************************
	 * Logic to create the list of Pending Users
	 * Takes the current activity as a parameter.
	 ***************************************************/
	public static void refreshPending(final Activity activity){
		final ListView pendingview = (ListView) activity.findViewById(R.id.pendingCarers);
		final ParseUser user = ParseUser.getCurrentUser();
		//create a loading dialog
		final ProgressDialog proDialog = new ProgressDialog(activity);
		proDialog.setMessage("loading...");
		proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		proDialog.show();
		
		LinkFunctions.getPendingUsers(user, new FindCallback<ParseObject>(){

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				List<Map<String, String>> list = LinkFunctions.getUsersDone(user, e, objects);
				
				//add the values to the list
				SimpleAdapter adapter = new SimpleAdapter(activity.getApplicationContext(), list, R.layout.list_item, new String[] {"name", "id"}, new int[] {R.id.user_name, R.id.user_id});
				pendingview.setAdapter(adapter);
				
				pendingview.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub
						final TextView item = (TextView) view.findViewById(R.id.user_id);
						new AlertDialog.Builder(activity)
						.setTitle("Remove Carer?")
						.setMessage("Do you really want to remove this carer request?")
						.setIcon(android.R.drawable.ic_dialog_alert)
						.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
						    public void onClick(DialogInterface dialog, int whichButton) {
						    	LinkFunctions.removeLink(item.getText().toString(), activity);
						    }})
						 .setNegativeButton(android.R.string.no,  new DialogInterface.OnClickListener() {
							    public void onClick(DialogInterface dialog, int whichButton) {
							    	dialog.dismiss();
							    }
						 }).show();
					}
					
				});
				
				//remove the loading dialog
				proDialog.dismiss();
			} 
		});
	}


}
