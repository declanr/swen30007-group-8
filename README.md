# README SWEN30007 Group 8

This readme is for the use of the Emergency Alarm Application running on Android 4.3 Operating System.

In order to use this application, you will need to Register as either an Assisted Person (if you wish to send alerts)  or a Carer (if you wish to receive alerts) and then Login to that User. 
An Assisted Person is able to initiate a request to link with a Carer through the User Management screen. The Assisted Person needs to know the email with which the Carer registered in order to send the request.
The Carer can then review their pending Link requests through their User Management screen. Pressing on an item in the 'Pending Users' list will bring up a dialog box asking the user to Accept or Reject the Link.
Once there exists an Accepted Link between two Users, the Assisted Person can initiate a communication with their Carers by pressing either the 'Alert' or 'I'm OK' buttons.
This will send a notification to the Carer's phone with the name of the Assisted Person and the nature of the Communication.

The tests for this applicaiton can be found in the aptly named MainActivityTest project. Running these tests will confirm that the queries and functions which contact the Parse Database are being received and executing in an expected manner.
