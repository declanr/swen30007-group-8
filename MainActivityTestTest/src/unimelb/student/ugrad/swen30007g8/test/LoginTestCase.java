package unimelb.student.ugrad.swen30007g8.test;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import unimelb.student.ugrad.swen30007g8.ParseFunctions;
import unimelb.student.ugrad.swen30007g8.User;
import android.test.AndroidTestCase;
import android.widget.EditText;

public class LoginTestCase extends AndroidTestCase {
	
	public void testLogin() {
		ParseFunctions.parseInit(getContext());
		final EditText email1 = new EditText(getContext());
		email1.setText("ap@example.com");
		final EditText email2 = new EditText(getContext());
		email2.setText("carer@example.com");
		final EditText password1 = new EditText(getContext());
		password1.setText("ap1");
		final EditText password2 = new EditText(getContext());
		password2.setText("carer1");
		
		/***************************************************
		 * Check that login works for ap user
		 ***************************************************/
		User.checkLogin(email1, password1, new LogInCallback(){

			@Override
			public void done(ParseUser user, ParseException e) {
				assertTrue(user.getObjectId().equals("QxKCjSLMkp"));
				assertFalse(user.getBoolean("isCarer"));
			}
		});
		
		/***************************************************
		 * Check that login works for carer user
		 ***************************************************/
		User.checkLogin(email2, password2, new LogInCallback(){

			@Override
			public void done(ParseUser user, ParseException e) {
				assertTrue(user.getObjectId().equals("mvAKTrmFR5"));
				assertTrue(user.getBoolean("isCarer"));
			}
		});
	}

}
