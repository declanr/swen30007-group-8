package unimelb.student.ugrad.swen30007g8.test;

import java.util.List;
import java.util.Map;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import unimelb.student.ugrad.swen30007g8.LinkFunctions;
import unimelb.student.ugrad.swen30007g8.ParseFunctions;
import unimelb.student.ugrad.swen30007g8.User;
import android.test.AndroidTestCase;
import android.widget.EditText;

public class LinkTestCase extends AndroidTestCase {
	
	public void testLink1() {
		/**
		 * Not a whole lot to test for this one, just making sure that the queries are all as expected.
		 * 
		 * Considered successful if the corresponding user in each of 2 hard-coded links are correctly pulled from the database
		 */
		ParseFunctions.parseInit(getContext());
		//First User = AP in link that is Pending
		//has 0 accepted links
		//has 1 pending link
		final ParseUser apf = new ParseUser();
		apf.put("isCarer", false);
		apf.setObjectId("testAPIdFALSE");
		//0 accepted
		LinkFunctions.getLinkedUsers(apf, new FindCallback<ParseObject>(){
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				List<Map<String, String>> links = LinkFunctions.getUsersDone(apf, e, objects);
				assertTrue(links.size() == 1);
				assertTrue(links.get(0).get("name").equals("No Users Found"));
			}
		});
		//1 pending
		LinkFunctions.getPendingUsers(apf, new FindCallback<ParseObject>(){
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				List<Map<String, String>> links = LinkFunctions.getUsersDone(apf, e, objects);
				assertTrue(links.size() == 1);
				System.out.println("AAAAA "+links.get(0).get("name"));
				assertTrue(links.get(0).get("name").equals("testCarerNameFALSE"));
			}
		});
	}
	public void testLink2() {
		/**
		 * Not a whole lot to test for this one, just making sure that the queries are all as expected.
		 * 
		 * Considered successful if the corresponding user in each of 2 hard-coded links are correctly pulled from the database
		 */
		ParseFunctions.parseInit(getContext());
		//First User = AP in link that is Pending
		//has 1 accepted link
		//has 0 pending links
		final ParseUser apt = new ParseUser();
		apt.put("isCarer", false);
		apt.setObjectId("testAPIdTRUE");
		//1 accepted
		LinkFunctions.getLinkedUsers(apt, new FindCallback<ParseObject>(){
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				List<Map<String, String>> links = LinkFunctions.getUsersDone(apt, e, objects);
				assertTrue(links.size() == 1);
				assertTrue(links.get(0).get("name").equals("testCarerNameTRUE"));
			}
		});
		//1 pending
		LinkFunctions.getPendingUsers(apt, new FindCallback<ParseObject>(){
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				List<Map<String, String>> links = LinkFunctions.getUsersDone(apt, e, objects);
				assertTrue(links.size() == 1);
				assertTrue(links.get(0).get("name").equals("No Users Found"));
			}
		});
	}
}
