package unimelb.student.ugrad.swen30007g8.test;

import java.security.SecureRandom;
import java.math.BigInteger;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import unimelb.student.ugrad.swen30007g8.ParseFunctions;
import unimelb.student.ugrad.swen30007g8.User;
import android.test.AndroidTestCase;
import android.widget.EditText;

public class RegisterTestCase extends AndroidTestCase {
	
	public void testRegister() throws ParseException, InterruptedException {
		ParseFunctions.parseInit(getContext());
		SecureRandom r = new SecureRandom();
		final String randomemail = new BigInteger(130, r).toString(32) + "@registertesting.com";
		
		final EditText email1 = new EditText(getContext());
		email1.setText(randomemail);
		final EditText password = new EditText(getContext());
		password.setText("test123");
		
		System.out.println(randomemail);
		/***************************************************
		 * Register a user with a random email
		 ***************************************************/
		ParseFunctions.register("Register Testing", randomemail, "test123", false, new SignUpCallback(){

			@Override
			public void done(ParseException e) {
				
			}
			
		});
		
		/***************************************************
		 * Need to sleep here since it's multi-threaded
		 ***************************************************/
		Thread.sleep(3000);
		
		/***************************************************
		 * Check that login works for the new user
		 ***************************************************/
		User.checkLogin(email1, password, new LogInCallback(){

			@Override
			public void done(ParseUser user, ParseException e) {
				assertTrue(user.getEmail().equals(randomemail));
			}
		});
	}

}
